import { expect, tap } from '@pushrocks/tapbundle';
import * as smartmail from '../ts/index.js';

let emailAddressValidatorInstance: smartmail.EmailAddressValidator;

tap.test('should create an instance of EmailAddressValidator', async () => {
  emailAddressValidatorInstance = new smartmail.EmailAddressValidator();
  expect(emailAddressValidatorInstance).toBeInstanceOf(smartmail.EmailAddressValidator);
});

tap.test('should validate an email', async () => {
  const result = await emailAddressValidatorInstance.validate('sandbox@bleu.de');
  expect(result.freemail).toBeFalse();
  expect(result.disposable).toBeFalse();
  console.log(result);
});

tap.test('should recognize an email as freemail', async () => {
  const result = await emailAddressValidatorInstance.validate('sandbox@gmail.com');
  expect(result.freemail).toBeTrue();
  expect(result.disposable).toBeFalse();
  console.log(result);
});

tap.test('should recognize an email as disposable', async () => {
  const result = await emailAddressValidatorInstance.validate('sandbox@gmx.de');
  expect(result.freemail).toBeFalse();
  expect(result.disposable).toBeTrue();
  console.log(result);
});

tap.test('should create a SmartMail', async () => {
  const testSmartmail = new smartmail.Smartmail({
    body: 'hi there',
    from: 'noreply@mail.lossless.com',
    subject: 'hi from here',
  });
  expect(testSmartmail).toBeInstanceOf(smartmail.Smartmail);
});

tap.start();
