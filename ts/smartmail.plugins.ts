// node native scope
import * as path from 'path';

export { path };

// pushrocks scope
import * as smartdns from '@pushrocks/smartdns';
import * as smartfile from '@pushrocks/smartfile';
import * as smartmustache from '@pushrocks/smartmustache';
import * as smartpath from '@pushrocks/smartpath';
import * as smartrequest from '@pushrocks/smartrequest';

export { smartdns, smartfile, smartmustache, smartpath, smartrequest };
