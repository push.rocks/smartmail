/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartmail',
  version: '1.0.23',
  description: 'a unified format for representing and dealing with mails'
}
